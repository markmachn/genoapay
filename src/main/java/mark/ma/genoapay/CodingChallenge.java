package mark.ma.genoapay;

/**
 * Mark.Ma
 * 13/03/19 8:12 PM
 **/

public class CodingChallenge {
    //constant variables for error msg
    private static final String ERR_EMPTY_INPUT = "input stock prices cannot be empty";
    private static final String ERR_INPUT = "input stock prices are wrong";
    private static final String ERR_NON_POSITIVE_INPUT = "input stock prices cannot be non-positive numbers";

    public int getMaxProfit(int[] stockPrices) {
        //conditions check
        if (null == stockPrices || stockPrices.length == 0) {
            throw new IllegalArgumentException(ERR_EMPTY_INPUT);
        }

        if (stockPrices.length < 2) {
            throw new IllegalArgumentException(ERR_INPUT);
        }

        if (checkIsPositive(stockPrices[0])) {
            throw new IllegalArgumentException(ERR_NON_POSITIVE_INPUT);
        }

        //init the max profit value, max is the max profit value of array in the scope from 0 to i
        int max = stockPrices[1] - stockPrices[0];
        //init the min value in the array, min is the min value of array in the scope from 0 to i
        int min = stockPrices[0];
        for (int i = 1; i < stockPrices.length; i++) {
            if (checkIsPositive(stockPrices[i])) {
                throw new IllegalArgumentException(ERR_NON_POSITIVE_INPUT);
            }
            //the max profit value of array in scope from 0 to i+1 should be the max value between the (array[i+1] - min) and max
            max = max(stockPrices[i] - min, max);
            //update the min value
            min = min(min, stockPrices[i]);
        }

        //if max profit value is less than 0 return 0
        return max > 0 ? max : 0;
    }

    private boolean checkIsPositive(int price) {
        return price < 1;
    }

    private int max(int i, int j) {
        return i > j ? i : j;
    }

    private int min(int i, int j) {
        return i < j ? i : j;
    }
}
