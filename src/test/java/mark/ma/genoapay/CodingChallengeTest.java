package mark.ma.genoapay;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * Mark.Ma
 * 13/03/19 8:12 PM
 **/

public class CodingChallengeTest {
    private CodingChallenge cc;

    @Before
    public void init() {
        cc = new CodingChallenge();
    }

    @Test(expected = IllegalArgumentException.class)
    public void emptyInputShouldReturnErr() {
        int[] stockPrices = {};
        cc.getMaxProfit(stockPrices);
    }

    @Test(expected = IllegalArgumentException.class)
    public void just1PriceInputShouldReturnErr() {
        int[] stockPrices = {10};
        cc.getMaxProfit(stockPrices);
    }

    @Test(expected = IllegalArgumentException.class)
    public void nonPositivePriceInputShouldReturnErr() {
        int[] stockPrices = {10, -7, 5, 8, 11, 9};
        cc.getMaxProfit(stockPrices);
    }

    @Test
    public void nonPositiveProfitReturnZero() {
        int[] stockPrices = {6, 5, 4, 3, 2, 1};
        Assert.assertEquals(0, cc.getMaxProfit(stockPrices));
    }

    @Test
    public void rightInputWithRightReturn() {
        int[] stockPrices = {10, 7, 5, 8, 11, 9};
        Assert.assertEquals(6, cc.getMaxProfit(stockPrices));
    }

    @Test
    public void rightInputWithRightReturn1() {
        int[] stockPrices = {1, 2, 3, 4, 5, 6};
        Assert.assertEquals(5, cc.getMaxProfit(stockPrices));
    }

    @Test
    public void rightInputWithRightReturn2() {
        int[] stockPrices = {1, 2, 1, 2, 1, 2, 1, 2};
        Assert.assertEquals(1, cc.getMaxProfit(stockPrices));
    }

    @Test
    public void sameInputWithRightReturn() {
        int[] stockPrices = {1, 1, 1, 1, 1, 1, 1, 1};
        Assert.assertEquals(0, cc.getMaxProfit(stockPrices));
    }

    @Test
    public void maxPriceInputWithRightReturn() {
        int[] stockPrices = {1, 6, 4, 5, 3, 123, Integer.MAX_VALUE};
        Assert.assertEquals(Integer.MAX_VALUE - 1, cc.getMaxProfit(stockPrices));
    }
}
